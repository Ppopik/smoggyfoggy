# README #

Repozytorium aplikacji: Monitor jakości powietrza

### Podstawowe wiadomośći ###

* pobieranie danych z API (https://powietrze.gios.gov.pl/pjp/content/api)
* wizualizację otrzymanych danych z API (np. mapa/wykres/obrazek/lista/ikona itp.)

### Aplikacja SmoggyFoggy ###

Aplikacja ma za zadanie przedstawie indeksu jakości powietrza w wybranym przez użtkownika miejscu. Dane na temat powietrza pobierane są za pomocą API udostępnionego przez "Inspekcja Ochrony Środowiska".
Co udało się osiągnąć:

* Pobieranie danych z API w postaci JSONA
* Przetwarzanie otrzymanych danych oraz wyświetlenie komunikatu na temat powietrza
* Wizualizacja jakości powietrza za pomocą EMOJI 🙂
* Stworzona aplikacja jest responsywna

### Jak wygląda aplikacja?  ###

Ekran głowny aplikacji (wybierania miasta) :

![Scheme](screenshot/IMG_3860.PNG)

Gdy zostanie wybrane miejsce, należy użyć przyciku "OK" w celu przejścia do ekranu wyświetlającej indeks jakości powietrza.

Ekran pokazujący jakość powietrza:

Przykład dla Wrocław:

![Scheme](screenshot/IMG_3861.PNG)

Przykład dla Częstochowa:

![Scheme](screenshot/IMG_3862.PNG)

Z tego ekranu jest możliwe cofnięcie do ekranu wyboru miejsca pomiaru.

### Jak można rozwinąć projekt? ###

Stworzona aplikacja wykonuje przedstawione jej zadanie, ale jest możliwość rozwinięcia danego projektu:

* Automatyczne ładowanie dostępnych stacji pomiarowych
* Dodanie mapy
* Szczegółowe dane
* Dodanie ekranu z informacjami na temat zanieczyszczenia powietrza
* Dodanie informacji jak należy poprawić jakość powietrza, sposoby na zdrowe przebywanie w miejscu o złej jakości powietrza ( maski smogowe itp. )