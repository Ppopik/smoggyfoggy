//
//  AppDelegate.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 05.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        let url = URL(string: "http://api.gios.gov.pl/pjp-api/rest/station/findAll")!
//        let netProcsessor = NetProcessor(url: url)
//        netProcsessor.downloadJSON { (jsonDictionary) in
//            print(jsonDictionary)
//        }
        
        
        let apiService = ApiService()
        apiService.getData(stationId: 52) { (currentAir) in
            print(currentAir)
        }
        return true
    }

}

