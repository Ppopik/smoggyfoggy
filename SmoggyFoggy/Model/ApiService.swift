//
//  ApiService.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 05.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import Foundation


class ApiService{
    let UrlFindAll = URL(string: "http://api.gios.gov.pl/pjp-api/rest/station/findAll")
    // http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/{stationId}
    let UrlAqindexBase = URL(string: "http://api.gios.gov.pl/pjp-api/rest/aqindex/getIndex/")

    // Pobieranie listy miast wraz z numerem id , pierwszy ekran
    
    
    // Pobieranie danych na temat jakości powietrz
    func getData(stationId: Int, completion: @escaping (CurrentAir?) -> Void) {
        if let UrlAqindexURL = URL(string: "\(stationId)", relativeTo: UrlAqindexBase!) {
            let netProcessor = NetProcessor(url: UrlAqindexURL)
            netProcessor.downloadJSON({ (jsonDictionary) in
                if let currentAirDictionary = jsonDictionary?["stIndexLevel"] as? [String: Any] {
                    let currentAir = CurrentAir(airDictionary: currentAirDictionary)
                    completion(currentAir)
                } else {
                    completion(nil)
                }
            })
        }
    }
    
    
}
