//
//  City.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 06.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import Foundation

class City {
    var name: String
    var id: Int
    
    init?(name: String, id: Int) {
        if name.isEmpty || id < 0 {
            return nil
        }
        
        self.name = name
        self.id = id

    }
}
