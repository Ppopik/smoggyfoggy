//
//  AqIndex.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 05.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import Foundation

class CurrentAir {
    let indexLevelName: String?
    
    init(airDictionary: [String: Any]){
        indexLevelName = airDictionary["indexLevelName"] as? String
    }
}
