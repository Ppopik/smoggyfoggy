//
//  NetProccesor.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 05.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import Foundation

class NetProcessor {
    // lazy w celu uzycia w odpowiednim momencie
    lazy var configuration: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: configuration)
    
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    typealias JSON = (([String: Any]?) -> Void)
    func downloadJSON(_ completion: @escaping JSON) {
        let request = URLRequest(url: self.url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            
            if error == nil {
                
                if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                    case 200:
                        // ok
                        if let data = data {
                            do{
                                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                                completion(jsonDictionary as? [String: Any])
                            } catch let error as NSError {
                               // print("Error, przetwarzanie jsona: \(error?.localizedDescription)")
                            }
                        }
                    default:
                        print("Kod HTTP: \(httpResponse.statusCode)")
                    }
                }
                
            } else {
                print("Error: \(error?.localizedDescription)")
            }
        }
        dataTask.resume()
    }
}
