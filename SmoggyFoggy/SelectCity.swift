//
//  SelectCity.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 05.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import UIKit

class SelectCity: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    var cities = [City]()
    var selectedRow: Int = 0

    @IBOutlet weak var pickerView: UIPickerView!



    
    override func viewDidLoad() {
//        pickerView.isHidden = true
        loadData()
        pickerView.delegate = self
        pickerView.dataSource = self
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Private method
    // wprowadzanie danych na sztywno ☹️
    private func loadData() {
        guard let city1 = City(name: "Wrocław", id: 117) else {
            fatalError("Upss, coś nie tak z ładowaniem danych 😟")
        }
        guard let city2 = City(name: "Łódź", id: 10058) else {
            fatalError("Upss, coś nie tak z ładowaniem danych 😟")
        }
        guard let city3 = City(name: "Częstochowa", id: 800) else {
            fatalError("Upss, coś nie tak z ładowaniem danych 😟")
        }
        guard let city4 = City(name: "Kraków", id: 10123) else {
            fatalError("Upss, coś nie tak z ładowaniem danych 😟")
        }

        cities += [city1,city2,city3,city4]
    }
    
    
    // Action
    
    @IBAction func selectButton(_ sender: UIButton) {
        
    }
    
    // Przesyłanie danych
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var secondController = segue.destination as! ViewController
        secondController.selectedCity = cities[selectedRow]
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent: Int) -> String? {
        // wybieranie miasta
        let city = cities[row]
        selectedRow = row
        return city.name
    }
    

    
}
