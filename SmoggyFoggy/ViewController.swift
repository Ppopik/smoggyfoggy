//
//  ViewController.swift
//  SmoggyFoggy
//
//  Created by Kamil Popczyk on 05.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var cityTextLabel: UILabel!
    @IBOutlet weak var indexLevelLabel: UILabel!
    @IBOutlet weak var emojiLabel: UILabel!
    
    var selectedCity = City(name: "Coś nie tak", id: 0)
    
    
    // POPRAWIC !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    override func viewDidLoad() {
        
        
//        var id = 331
//        if let selectedCity = selectedCity{
//            print(selectedCity.name)
//            cityTextLabel.text = selectedCity.name
//            id = selectedCity.id
//        }
        
        cityTextLabel.text = selectedCity?.name
        
        let apiService = ApiService()
        apiService.getData(stationId: (selectedCity?.id)!) { (currentAir) in
            if let currentAir = currentAir {
                DispatchQueue.main.async {
                    if let indexLevelName = currentAir.indexLevelName {
                        self.indexLevelLabel.text = "\(indexLevelName)"
                        // emoji zalezne od wartosci
                        switch indexLevelName {
                        case "Bardzo dobry":
                            self.emojiLabel.text = "😃"
                        case "Dobry":
                            self.emojiLabel.text = "🙂"
                        case "Umiarkowany":
                            self.emojiLabel.text = "😐"
                            // Dodać dalesze przypadki !
                        default:
                            self.emojiLabel.text = "😷"
                        }
                    } else {
                        self.indexLevelLabel.text = "Coś nie tak z JSON"
                    }

                    
                    
                }
            }
        }
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

